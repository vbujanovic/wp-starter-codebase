/* ==========================================================================
 SEN SLIDESHOW
 ========================================================================== */

(function ($, undefined) {

    'use strict';

    var _super = $.Widget.prototype;

    $.widget('sen.slideshow', {

        version: '1.0',

        options: {
            slide: '.slide',
            nav: '.slideshow-nav',
            prev: '.prev-btn',
            next: '.next-btn',
            auto: true,
            interval: 5000
        },
        /**
         *
         * @returns {*}
         * @private
         */
        _create: function () {
            var base = this;

            if (!Modernizr.csstransitions) {
                this.element.addClass('no-transitions');
            }

            if (this.options.auto === true) {
                this._addInterval();
            }

            this.slides = this.element.find(this.options.slide);
            this.currentSlide = this.slides.eq(0).addClass('current');


            //Generate navigation if it's required
            if (this.options.nav !== false) {

                this.navigation = $('<div class="' + this.options.nav.substr(1) + '"></div>');
                this.element.append(this.navigation);

                this.slides.each(function (index) {
                    if (index === 0) {
                        base.navigation.append('<a href="#" class="nav-btn current"><span>' + index + '</span></a>');
                    } else {
                        base.navigation.append('<a href="#" class="nav-btn"><span>' + index + '</span></a>');
                    }
                });
            }

            this._bindEvents();

            return this;
        },
        /**
         * Add event listeners
         * @private
         */
        _bindEvents: function () {
            var base = this;

            this.element.on('click touchstart', this.options.next, $.proxy(this._nextSlide, this));

            this.element.on('click touchstart', this.options.prev, $.proxy(this._prevSlide, this));

            this.element.on('click touchstart', '.nav-btn', $.proxy(this._navSlide, this));


            if (jQuery.event.special.swipe) {

                this.element.on('swiperight', $.proxy(this._nextSlide, this));

                this.element.on('swipeleft', $.proxy(this._prevSlide, this));

                this.element.on('movestart', function (e) {
                    // If the movestart is heading off in an upwards or downwards
                    // direction, prevent it so that the browser scrolls normally.
                    if ((e.distX > e.distY && e.distX < -e.distY) ||
                        (e.distX < e.distY && e.distX > -e.distY)) {
                        e.preventDefault();
                    }
                });
            }
        },

        _addInterval: function () {
            var base = this;

            this.changeInterval = setInterval(function () {
                base._nextSlide();
            }, this.options.interval);
        },

        _removeInterval: function () {
            clearInterval(this.changeInterval);
        },


        _navSlide : function(e){
            var base = this;

            e.preventDefault();

            base._removeInterval();

            var index = $(e.currentTarget).index();

            base._goToSlide(index);
        },
        /**
         *
         * @private
         */
        _nextSlide: function () {
            var index;

            this._removeInterval();

            index = this.currentSlide.removeClass('current').next(this.options.slide).index();

            if (index === -1) {
                index = 0;
            }

            this._goToSlide(index);

            this._trigger('onNext', window.event, {
                element: this.element
            });
        },

        _prevSlide: function () {
            var index;

            this._removeInterval();

            index = this.currentSlide.removeClass('current').prev(this.options.slide).index();

            if (index === -1) {
                index = this.slides.length - 1;
            }

            this._goToSlide(index);

            this._trigger('onPrev', window.event, {
                element: this.element
            });
        },

        _goToSlide: function (slide) {

            this.currentSlide.removeClass('current');

            this.currentSlide = this.slides.eq(slide).addClass('current');

            if (this.options.nav) {
                this.navigation
                    .find('.nav-btn')
                    .removeClass('current')
                    .eq(slide)
                    .addClass('current');
            }

            this._addInterval();
        },

        /**
         * Destroy instance
         */
        destroy: function () {

            _super.destroy.apply(this, arguments);

            return;
        }

    });

})(jQuery, undefined);
