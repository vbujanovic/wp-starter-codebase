/* ==========================================================================
 SEN TABS
 ========================================================================== */
(function ($, undefined) {

    'use strict';

    var _super = $.Widget.prototype;

    $.widget('sen.tabs', {

        version: '1.0',
        tabs: [],
        useHash: false,
        options: {
            tabsNav: '.tab-btn',
            accordionNav: '.accordion-btn',
            content: '.tab',
            active: 'active',
            animated: false,
            responsive: true
        },

        /**
         *
         * @returns {*}
         * @private
         */
        _create: function () {
            var base = this;

            this.nav = this.element.find(this.options.tabsNav);
            this.ctn = this.element.find(this.options.content);

            this.nav.each(function (i) {
                var $this = $(this),
                    tabName = $this.data('tab'),
                    temp = base.options.accordionNav,
                    html = '<span class="' + temp.substring(1, temp.length) + '" data-tab="' + tabName + '">' + $this.text() + '</span>';

                base.tabs.push(tabName);

                if (base.options.responsive == true) {
                    $(html).insertBefore(base.ctn.eq(i));
                }
            });

            this.mobileNav = this.element.find(this.options.accordionNav);

            if (this.options.animated) {
                this.ctn.addClass('animated');
            }

            this._bindEvents();

            this.changeTo(window.location.hash.slice(1));

            return this;
        },
        /**
         * @private
         */
        _bindEvents: function () {
            var base = this;

            this.element.on('click', this.options.tabsNav, function (e) {
                e.preventDefault();

                base.last = $(this).data('tab');
                base.changeTo(base.last);
            });

            this.element.on('click', this.options.accordionNav, function (e) {
                e.preventDefault();
                e.stopPropagation();

                var name = $(this).data('tab');

                if (base.last === name) {
                    base.last = '';

                    base.mobileNav.removeClass(base.options.active);
                    base.ctn.removeClass(base.options.active);

                } else {
                    base.last = name;
                    base.changeTo(base.last);
                }

                /*
                * Start from top on open
                * */
                $('html, body').animate({
                    scrollTop: $(this).offset().top
                }, 0);
            });
        },


        /**
         * @param curNav
         * @param selectedCtn
         * @param tabName
         * @private
         */
        _change: function (selectedNav, selectedMobileNav, selectedCtn, tabName) {
            var base = this;

            this.nav.removeClass(base.options.active);
            this.ctn.removeClass(base.options.active);
            this.mobileNav.removeClass(base.options.active);

            selectedNav.addClass(base.options.active);
            selectedCtn.addClass(base.options.active);
            selectedMobileNav.addClass(base.options.active);

            if (this.options.useHash === true) {
                window.location.hash = tabName;
            }

            this._trigger('onChange', window.event, {
                element: base.element
            });
        },
        /**
         * @param tabName
         */
        changeTo: function (tabName) {
            var nav, ctn, mnav;

            if (tabName && $.inArray(tabName, this.tabs) > -1) {
                nav = this.nav.filter('[data-tab="' + tabName + '"]');
                ctn = this.ctn.filter('[data-tab="' + tabName + '"]');
                mnav = this.mobileNav.filter('[data-tab="' + tabName + '"]');

            } else {
                nav = this.nav.eq(0);
                ctn = this.ctn.eq(0);
                mnav = this.mobileNav.eq(0);

                this.last = this.mobileNav.eq(0).data('tab');
            }

            this._change(nav, mnav, ctn, tabName);
        },
        /**
         * @returns {*}
         */
        destroy: function () {

            _super.destroy.apply(this, arguments);

            return this;
        }

    });

})(jQuery);
