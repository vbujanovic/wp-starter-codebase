/* ==========================================================================
 SEN ACCORDION
 ========================================================================== */

(function ($, undefined) {

    'use strict';

    var _super = $.Widget.prototype;

    $.widget('sen.accordion', {

        version: '1.0',

        options: {
            wrapper: 'section',
            heading: '.heading',
            content: '.content'
        },
        /**
         *
         * @returns {*}
         * @private
         */
        _create: function () {

            if (!Modernizr.csstransitions) {
                this.element.addClass('no-transitions');
            }

            this._bindEvents();

            return this;
        },
        /**
         * Add event listeners
         * @private
         */
        _bindEvents: function () {

            this.element.on('click touchstart', this.options.heading, $.proxy(this._toggleContent, this));
        },
        /**
         *
         * @param e
         * @private
         */
        _toggleContent: function (e) {
            var el = $(e.currentTarget).closest(this.options.wrapper);

            e.preventDefault();

            if (el.hasClass('opened')) {

                el.removeClass('opened');
                el.one('transitionend webkitTransitionEnd oTransitionEnd otransitionend MSTransitionEnd', $.proxy(this._onClose, this));

                if (!Modernizr.csstransitions) {
                    el.find(this.options.content).stop().slideUp('normal', $.proxy(this._onClose, this));
                }

            } else {

                el.addClass('opened');
                el.one('transitionend webkitTransitionEnd oTransitionEnd otransitionend MSTransitionEnd', $.proxy(this._onOpen, this));

                if (!Modernizr.csstransitions) {
                    el.find(this.options.content).stop().slideDown('normal', $.proxy(this._onOpen, this))
                }
            }
        },
        /**
         *
         * @private
         */
        _onClose : function(){

            this._trigger('onClose', window.event, {
                element: this.element
            });
        },
        /**
         *
         * @private
         */
        _onOpen : function(){

            this._trigger('onOpen', window.event, {
                element: this.element
            });
        },
        /**
         * Destroy instance
         */
        destroy: function () {

            _super.destroy.apply(this, arguments);

            return;
        }

    });

})(jQuery);
