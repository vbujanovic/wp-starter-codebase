/* ==========================================================================
 SEN MODAL
 ========================================================================== */

(function ($, undefined) {

    'use strict';

    var _super = $.Widget.prototype;

    $.widget('sen.modal', {

        version: '1.0',

        options: {
            btn: '.modal-btn',
            maxWidth: 320,
            minWidth: 280
        },
        /**
         * @returns {*}
         * @private
         */
        _create: function () {

            this.shadow = $('<div class="modal-mask"></div>').insertAfter(this.element);
            this.ctn = $('<div class="modal-inner-wrapper"></div>').appendTo(this.element);
            this.closeBtn = $('<span class="modal-close"></span>').appendTo(this.element);

            this.element.css({
                minWidth: this.options.minWidth,
                maxWidth: this.options.maxWidth
            })

            this.transitionEnd = 'transitionend webkitTransitionEnd oTransitionEnd otransitionend MSTransitionEnd';

            if (!Modernizr.csstransitions) {
                this.element.addClass('no-transitions');
                this.shadow.addClass('no-transitions');
            }

            this._bindEvents();

            return this;
        },
        /**
         * @private
         */
        _bindEvents: function () {
            var base = this;

            this.element.on('click', '.modal-close', $.proxy(this.close, this));

            this.shadow.on('click', $.proxy(this.close, this));

            $(document).on('click', this.options.btn, $.proxy(this._loadContent, this));

            $(window).resize(function () {
                base.centerDialog();
            });

            $(document).keyup(function (e) {
                // esc key pressed
                if (e.keyCode == 27) {
                    base.close();
                }
            });
        },
        /**
         *
         * @param e
         * @returns {boolean}
         * @private
         */
        _loadContent: function (e) {
            var base = this,
                target = this.source = $(e.currentTarget);

            e.preventDefault();

            $.ajax({
                type: "GET",
                url: target.attr('href'),
                cache: false,
                async: false,
                success: function (response) {
                    base.open(response);
                },
                error: function (error) {
                    console.error(error);
                }
            });

            return false;
        },
        /**
         *
         * @private
         */
        _onOpen: function () {

            this._trigger('onOpen', window.event, {
                element: this.element,
                target: this.source
            });

            this.element.off(this.transitionEnd);
        },
        /**
         *
         * @private
         */
        _onClose: function () {

            this.ctn.html('');
            this.shadow.css({
                width: 0,
                height: 0
            });

            this._trigger('onClose', window.event, {
                element: this.element
            });

            this.element.off(this.transitionEnd);
        },
        /**
         *
         * @returns {*}
         */
        reload: function () {
            this.source.trigger('click');

            return this;
        },
        /**
         *
         * @returns {*}
         */
        centerDialog: function () {

            if (!this.element.hasClass('active')) {
                return false;
            }

            var ww = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth,
                wh = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight,
                left = ww >= this.options.maxWidth ? ww / 2 - this.element.width() / 2 : 0,
                top = (wh / 2) - (this.element.height() / 2) + $(window).scrollTop();

            this.element.css({
                'left': left,
                'top': top
            });

            this.shadow.css({
                width: ww,
                height: wh
            });

            return this;
        },
        /**
         *
         * @param html
         * @returns {*}
         */
        open: function (html) {

            this.ctn.html(html);
            this.element.addClass('active');
            this.shadow.addClass('active');

            this.centerDialog();

            if (!Modernizr.csstransitions) {

                this.shadow.show();
                this.element.fadeIn('normal', $.proxy(this._onOpen, this));

            } else {
                this.element.one(this.transitionEnd, $.proxy(this._onOpen, this));
            }

            return this;
        },
        /**
         *
         * @returns {*}
         */
        close: function (e) {

            e.stopPropagation();

            this.element.removeClass('active')
            this.shadow.removeClass('active')

            if (!Modernizr.csstransitions) {

                this.element.fadeOut('normal', $.proxy(function () {
                    this.shadow.hide();

                }, this));

            } else {
                this.element.one(this.transitionEnd, $.proxy(this._onClose, this));
            }

            return this;
        },
        /**
         * Destroy instance
         */
        destroy: function () {

            _super.destroy.apply(this, arguments);

            return;
        }

    });

})(jQuery);
