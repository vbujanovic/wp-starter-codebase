/* ==========================================================================
 SEN NAVIGATION
 ========================================================================== */

(function ($, undefined) {

    'use strict';

    var _super = $.Widget.prototype;

    $.widget('sen.navigation', {

        version: '1.0',

        options: {
            pullBtn: '#pull-btn',
            childrenBtn: '.page_item_has_children',
            childrenList: '.children',
            maxWidth: 767,
            onOpen: $.noop,
            onClose: $.noop
        },
        /**
         * @returns {*}
         * @private
         */
        _create: function () {

            this.list = this.element.find('ul:not(' + this.options.childrenList + ')');

            this._bindEvents();
            this._toggleType();

            return this;
        },
        /**
         * @private
         */
        _bindEvents: function () {

            this.element.on('touchstart click', this.options.pullBtn, $.proxy(this._toggleNav, this));

            this.element.on('touchstart click', this.options.childrenBtn, $.proxy(this._toggleChild, this));

            $(window).on('resize', $.proxy(this._toggleType, this));
        },
        /**
         * @private
         */
        _toggleType: function () {

            if ($(window).outerWidth() > this.options.maxWidth) {
                this.element.addClass('desktop-nav');
            } else {
                this.element.removeClass('desktop-nav');
            }
        },

        /**
         * @param e
         * @private
         */
        _toggleNav: function (e) {
            e.preventDefault();
            e.stopImmediatePropagation();
            this.list.stop().slideToggle();
        },
        /**
         * @param e
         * @private
         */
        _toggleChild: function (e) {
            e.preventDefault();
            e.stopImmediatePropagation();

            $(e.currentTarget)
                .next(this.options.childrenList)
                .stop()
                .slideToggle();
        },
        /**
         * @returns {*}
         */
        destroy: function () {
            _super.destroy.apply(this, arguments);

            return this;
        }

    });

})(jQuery);
