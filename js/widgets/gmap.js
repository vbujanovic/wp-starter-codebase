/* ==========================================================================
 SEN GMAP
 ========================================================================== */

(function ($, undefined) {

    'use strict';

    var _super = $.Widget.prototype;

    $.widget('sen.gMap', {

        version: '1.0',

        options: {
            latitude: 0,
            longitude: 0,
            zoom: 1,
            markers: [],
            maptype: 'ROADMAP',
            info: true
        },

        /**
         *
         * @returns {*}
         * @private
         */
        _create: function () {
            var base = this, i;

            this.gmap = new google.maps.Map(this.element[0], {
                zoom: this.options.zoom,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            });

            this.gmarkers = [];

            if (base.options.info === true) {
                this.infowindow = new google.maps.InfoWindow({
                    content: "holding...",
                    maxWidth: 250
                });
            }

            for (i = 0; base.options.markers.length > i; i++) {
                var marker = this.options.markers[i];

                base.addMarker(marker.lat, marker.long, marker.html, marker.id)
            }

            this.centerAt(0);

            return this;
        },

        addMarker: function (lat, long, html, id) {
            var base = this, gmarker;

            gmarker = new google.maps.Marker({
                position: new google.maps.LatLng(lat, long),
                map: this.gmap,
                latitude: lat,
                longitude: long,
                title: '',
                id: id
            })

            gmarker.content = html;

            google.maps.event.addListener(gmarker, 'click', function () {

                base._trigger('beforeOpen', window.event, {
                    marker: this,
                    id: this.id
                });

                if (base.options.info === true) {
                    base.infowindow.setContent(this.content);
                    base.infowindow.open(base.gmap, this);
                }

            });

            this.gmarkers.push(gmarker);
        },

        centerAt: function (index, zoom) {
            if (zoom) {
                this.gmap.setZoom(zoom);
            }

            this.gmap.setCenter(this.gmarkers[index].getPosition());

        },

        /**
         * Destroy instance
         */
        destroy: function () {

            _super.destroy.apply(this, arguments);

            return;
        }

    });

})(jQuery);
