/* ==========================================================================
 MAIN
 ========================================================================== */

(function ($) {
    /**
     * Cache page wrapper element
     * @type {*|HTMLElement}
     */
    var element = $('#page-content');

    /**
     * Create page instance
     */
    switch (element.data('page')) {

//        case 'hompage':
//
//            page = element.page();
//
//            break;

        default:
            element.masterpage();
    }

})(jQuery);