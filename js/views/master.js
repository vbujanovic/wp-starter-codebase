/* ==========================================================================
 MASTER VIEW
 ========================================================================== */
(function ($, undefined) {

    'use strict';

    var _super = $.Widget.prototype;

    $.widget('sen.masterpage', {

        version: '1.0',

        options: {

        },
        /**
         *
         * @returns {*}
         * @private
         */
        _create: function () {

            /**
             * Set default jquery datepicker format
             */
            if ($.datepicker) {

                $.datepicker.setDefaults($.datepicker.regional['en-GB']);
                $.datepicker.setDefaults({ dateFormat: 'dd/mm/yy' });

                $('.date-field').datepicker();

                /**
                 * jQuery validation fix for datepicker UK format
                 */

                jQuery.validator.addMethod(
                    'date',
                    function (value, element, params) {
                        if (this.optional(element)) {
                            return true;
                        }
                        var result = false;
                        try {
                            $.datepicker.parseDate('dd/mm/yy', value);
                            result = true;
                        } catch (err) {
                            result = false;
                        }
                        return result;
                    },
                    ''
                );
            }

            /**
             * Create instances of placeholder object
             */
            if (!Modernizr.input.placeholder) {
                this.placeholder = $('input, textarea').placeholder();
            }

            /**
             * Create instances of navigation object
             */
            this.mainNav = $('#main-nav').navigation();

            /**
             * Create instances of tab object
             */
            this.tabs = $('#tabs').tabs();

            /**
             * Create instances of modal object
             */
            this.modal = $('.modal').modal();

            /**
             * Create instances of accordion object
             */
            this.accordion = $('#accordion').accordion();

            this.mainSlideshow = $('#main-slideshow').slideshow();

            /**
             * Bind events
             */
            this._bindEvents();

            /**
             * onCreate callback
             */
            this._trigger('onCreate', window.event, {
                element: this.element
            });

            return this;
        },

        _bindEvents: function () {

            /**
             * Hide address bar on mobile devices (except if #hash present, so we don't mess up deep linking).
             */
            if (Modernizr.touch && !window.location.hash) {
                $(window).on('load', function () {
                    setTimeout(function () {
                        window.scrollTo(0, 1);
                    }, 0);
                });
            }
        },

        /**
         * Destroy instance
         * @returns {*}
         */
        destroy: function () {

            _super.destroy.apply(this, arguments);

            return this;
        }

    });

})(jQuery);
