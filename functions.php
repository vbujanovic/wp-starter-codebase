<?php
require_once('libs/metabox-config.php');
require_once('libs/cpt.php');
require_once('libs/admin.php');

global $admin_options;  // This is your opt_name.
// Add RSS links to <head> section
automatic_feed_links();

// Load jQuery
if (!is_admin()) {
    wp_deregister_script('jquery');
    wp_register_script('jquery', ("http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"), false, false, true);
    wp_enqueue_script('jquery');

    wp_register_script('modernizr', get_template_directory_uri() . '/assets/js/libs/modernizr-2.6.2.min.js', false, false, false);
    wp_enqueue_script('modernizr');

    wp_register_script('scripts', get_template_directory_uri() . '/assets/js/scripts.js', false, false, true);
    wp_enqueue_script('scripts');

    /* wp_register_script('scripts', get_template_directory_uri() . '/assets/scripts/prod/scripts.min.js', false, false, true);
    wp_enqueue_script('scripts');  */

}

// Clean up the <head>
function removeHeadLinks()
{
    remove_action('wp_head', 'rsd_link');
    remove_action('wp_head', 'wlwmanifest_link');
}

add_action('init', 'removeHeadLinks');
remove_action('wp_head', 'wp_generator');

if (function_exists('register_sidebar')) {
    register_sidebar(array(
        'name' => 'Sidebar Widgets',
        'id' => 'sidebar-widgets',
        'description' => 'These are widgets for the sidebar.',
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget' => '</ul>',
        'before_title' => '<h2>',
        'after_title' => '</h2>',
        'after_widget' => '</div>',

    ));
}


/* Remove width and height attributes from wysiwyg images
 *
 */
add_filter( 'post_thumbnail_html', 'remove_width_attribute', 10 );
add_filter( 'image_send_to_editor', 'remove_width_attribute', 10 );

function remove_width_attribute( $html ) {
    $html = preg_replace( '/(width|height)="\d*"\s/', "", $html );
    return $html;
}
/*
 * Register Menu
 */
function register_my_menu()
{
    register_nav_menu('support-nav', __('Support Navigation'));
    register_nav_menu('main-nav', __('Main Navigation'));
    register_nav_menu('footer-nav', __('Footer Navigation'));
}

add_action('init', 'register_my_menu');

/*
 * Featured Image
 */

add_theme_support('post-thumbnails');


/*
 * Extra Styles
 */

// Callback function to insert 'styleselect' into the $buttons array
function my_mce_buttons_2($buttons)
{
    array_unshift($buttons, 'styleselect');
    return $buttons;
}

// Register our callback to the appropriate filter
add_filter('mce_buttons_2', 'my_mce_buttons_2');

function my_mce_before_init($init_array)
{
    $init_array['theme_advanced_styles'] =
        '.first=first;.readmore=readmore';


    return $init_array;
}

add_filter('tiny_mce_before_init', 'my_mce_before_init');


?>