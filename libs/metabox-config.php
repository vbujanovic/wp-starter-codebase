<?php
/*
    * Meta Boxes
    */
function wpcdbs_metaboxes($meta_boxes)
{
    $prefix = '_cmb_'; // Prefix for all fields

//    $meta_boxes['test_metabox'] = array(
//        'id' => 'test_metabox',
//        'title' => 'Test Metabox',
//        'pages' => array('page'), // post type
//        'context' => 'normal',
//        'priority' => 'high',
//        'show_names' => true, // Show field names on the left
//        'fields' => array(
//            array(
//                'name' => 'Test Text',
//                'desc' => 'field description (optional)',
//                'id' => $prefix . 'test_text',
//                'type' => 'text'
//            ),
//        ),
//    );
    return $meta_boxes;
}

add_filter('cmb_meta_boxes', 'wpcdbs_metaboxes');

// Initialize the metabox class
add_action('init', 'be_initialize_cmb_meta_boxes', 9999);
function be_initialize_cmb_meta_boxes()
{
    if (!class_exists('cmb_Meta_Box')) {
        require_once('metabox/init.php');
    }
}

/**
 * Exclude metabox on specific IDs
 * @author Bill Erickson
 * @link https://github.com/jaredatch/Custom-Metaboxes-and-Fields-for-WordPress/wiki/Adding-your-own-show_on-filters
 *
 * @param bool $display
 * @param array $meta_box
 * @return bool display metabox
 */
function be_metabox_exclude_for_id($display, $meta_box)
{
    if ('exclude_id' !== $meta_box['show_on']['key'])
        return $display;

    // If we're showing it based on ID, get the current ID
    if (isset($_GET['post'])) $post_id = $_GET['post'];
    elseif (isset($_POST['post_ID'])) $post_id = $_POST['post_ID'];
    if (!isset($post_id))
        return $display;

    // If current page id is in the included array, do not display the metabox
    $meta_box['show_on']['value'] = !is_array($meta_box['show_on']['value']) ? array($meta_box['show_on']['value']) : $meta_box['show_on']['value'];
    if (in_array($post_id, $meta_box['show_on']['value']))
        return false;
    else
        return true;
}

add_filter('cmb_show_on', 'be_metabox_exclude_for_id', 10, 2);