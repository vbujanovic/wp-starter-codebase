<?php
/*
    * Meta Boxes
    */
function wpcdbs_metaboxes($meta_boxes)
{
    $prefix = '_cmb_'; // Prefix for all fields

    $meta_boxes[] = array(
        'id' => 'home_page_slideshow',
        'title' => 'Home Slideshow',
        'pages' => array('page',), // Post type
        'show_on' => array('key' => 'id', 'value' => array(4)),
        'context' => 'normal',
        'priority' => 'high',
        'show_names' => false, // Show field names on the left
        'fields' => array(
            array(
                'desc' => 'Upload an image for slider.',
                'id' => $prefix . 'home_slide1',
                'type' => 'file',
                'save_id' => false,
                'allow' => array( 'url', 'attachment' )
            ),
            array(
                'desc' => 'Upload an image for slider.',
                'id' => $prefix . 'home_slide2',
                'type' => 'file',
                'save_id' => false,
                'allow' => array( 'url', 'attachment' )
            ),
            array(
                'desc' => 'Upload an image for slider.',
                'id' => $prefix . 'home_slide3',
                'type' => 'file',
                'save_id' => false,
                'allow' => array( 'url', 'attachment' )
            ),
            array(
                'desc' => 'Upload an image for slider.',
                'id' => $prefix . 'home_slide4',
                'type' => 'file',
                'save_id' => false,
                'allow' => array( 'url', 'attachment' )
            ),
            array(
                'desc' => 'Upload an image for slider.',
                'id' => $prefix . 'home_slide5',
                'type' => 'file',
                'save_id' => false,
                'allow' => array( 'url', 'attachment' )
            ),
        )
    );
    $meta_boxes[] = array(
        'id' => 'home_page_carousel',
        'title' => 'Home Carousel',
        'pages' => array('page',), // Post type
        'show_on' => array('key' => 'id', 'value' => array(4)),
        'context' => 'normal',
        'priority' => 'high',
        'show_names' => false, // Show field names on the left
        'fields' => array(
            array(
                'desc' => 'Upload an image for carousel.',
                'id' => $prefix . 'home_carousel1',
                'type' => 'file',
                'save_id' => false,
                'allow' => array( 'url', 'attachment' )
            ),
            array(
                'desc' => 'Upload an image for carousel.',
                'id' => $prefix . 'home_carousel2',
                'type' => 'file',
                'save_id' => false,
                'allow' => array( 'url', 'attachment' )
            ),
            array(
                'desc' => 'Upload an image for carousel.',
                'id' => $prefix . 'home_carousel3',
                'type' => 'file',
                'save_id' => false,
                'allow' => array( 'url', 'attachment' )
            ),
            array(
                'desc' => 'Upload an image for carousel.',
                'id' => $prefix . 'home_carousel4',
                'type' => 'file',
                'save_id' => false,
                'allow' => array( 'url', 'attachment' )
            ),
            array(
                'desc' => 'Upload an image for carousel.',
                'id' => $prefix . 'home_carousel5',
                'type' => 'file',
                'save_id' => false,
                'allow' => array( 'url', 'attachment' )
            ),
            array(
                'name' => 'Copy',
                'id' => $prefix . 'home_carousel_desc',
                'type' => 'wysiwyg',
                'options' => array(
                    'wpautop' => true, // use wpautop?
                ),
            ),
        )
    );

    $meta_boxes[] = array(
        'id' => 'home_red_box',
        'title' => 'Home Copy Boxes',
        'pages' => array('page',), // Post type
        'show_on' => array('key' => 'id', 'value' => array(4)),
        'context' => 'normal',
        'priority' => 'high',
        'show_names' => true, // Show field names on the left
        'fields' => array(
            array(
                'name' => 'Red Box',
                'type' => 'title',
                'id' => $prefix . 'red_box_title'
            ),
            array(
                'name' => 'Title',
                'id' => $prefix . 'red_box_title',
                'type' => 'text',
            ),
            array(
                'name' => 'Copy',
                'id' => $prefix . 'red_box_copy',
                'type' => 'wysiwyg',
                'options' => array(
                    'wpautop' => true, // use wpautop?
                ),
            ),
            array(
                'name' => 'Blue Box',
                'type' => 'title',
                'id' => $prefix . 'blue_box_title'
            ),
            array(
                'name' => 'Title',
                'id' => $prefix . 'blue_box_title',
                'type' => 'text',
            ),
            array(
                'name' => 'Copy',
                'id' => $prefix . 'blue_box_copy',
                'type' => 'wysiwyg',
                'options' => array(
                    'wpautop' => true, // use wpautop?
                ),
            ),
        )
    );


    $meta_boxes[] = array(
        'id' => 'article_page_metabox',
        'title' => 'Page Columns',
        'pages' => array('page', 'injuries', 'treatements'), // Post type
        'show_on' => array('key' => 'exclude_id', 'value' => array(5, 19)),
        'context' => 'normal',
        'priority' => 'high',
        'fields' => array(
            array(
                'name' => 'Left Column',
                'type' => 'title',
                'id' => $prefix . 'left_column_title'
            ),
            array(
                'name' => 'Left Column',
                'id' => $prefix . 'left_column',
                'type' => 'wysiwyg',
                'options' => array(
                    'textarea_rows' => 15,
                    'wpautop' => true, // use wpautop?
                ),
            ),
            array(
                'name' => 'Right Column',
                'id' => $prefix . 'right_column_title',
                'type' => 'title',
            ),
            array(
                'name' => 'Right Column',
                'id' => $prefix . 'right_column',
                'type' => 'wysiwyg',
                'options' => array(
                    'textarea_rows' => 15,
                    'wpautop' => true, // use wpautop?
                ),
            ),
        )
    );

    $meta_boxes[] = array(
        'id' => 'cta_page_metabox',
        'title' => 'CTA',
        'pages' => array('page', 'injuries', 'treatements'), // Post type
        'show_on' => array('key' => 'exclude_id', 'value' => array(5, 19)),
        'context' => 'normal',
        'priority' => 'high',
        'fields' => array(
            array(
                'name' => 'Title',
                'id' => $prefix . 'cta_title',
                'type' => 'text',
            ),
            array(
                'name' => 'Copy',
                'id' => $prefix . 'cta_copy',
                'type' => 'textarea_small'
            ),
        )
    );

    $meta_boxes[] = array(
        'id' => 'contact_metabox',
        'title' => 'Engandine',
        'pages' => array('page'), // Post type
        'show_on' => array('key' => 'id', 'value' => array(19)),
        'context' => 'normal',
        'show_names' => true, // Show field names on the left
        'priority' => 'high',
        'fields' => array(
            array(
                'name' => 'Title',
                'id' => $prefix . 'engandine_title',
                'type' => 'text_medium',
            ),
            array(
                'name' => 'Latitude',
                'id' => $prefix . 'engandine_lat',
                'type' => 'text_small',
            ),
            array(
                'name' => 'Longitude',
                'id' => $prefix . 'engandine_long',
                'type' => 'text_small',
            ),
            array(
                'name' => 'Address',
                'id' => $prefix . 'engandine_address',
                'type' => 'textarea_small'
            ),
        )
    );

    $meta_boxes[] = array(
        'id' => 'contacth_metabox',
        'title' => 'Helensburgh',
        'pages' => array('page'), // Post type
        'show_on' => array('key' => 'id', 'value' => array(19)),
        'context' => 'normal',
        'show_names' => true, // Show field names on the left
        'priority' => 'high',
        'fields' => array(
            array(
                'name' => 'Title',
                'id' => $prefix . 'helensburgh_title',
                'type' => 'text_medium',
            ),
            array(
                'name' => 'Latitude',
                'id' => $prefix . 'helensburgh_lat',
                'type' => 'text_small',
            ),
            array(
                'name' => 'Longitude',
                'id' => $prefix . 'helensburgh_long',
                'type' => 'text_small',
            ),
            array(
                'name' => 'Address',
                'id' => $prefix . 'helensburgh_address',
                'type' => 'textarea_small'
            ),
        )
    );

    $meta_boxes[] = array(
        'id' => 'testimonial_metabox',
        'title' => 'Subtitle',
        'pages' => array('testimonials'), // Post type
        'context' => 'normal',
        'show_names' => false, // Show field names on the left
        'priority' => 'high',
        'fields' => array(
            array(
                'name' => 'Subtitle',
                'id' => $prefix . 'test_subtitle',
                'type' => 'text_medium',
            ),
        )
    );

    return $meta_boxes;
}

add_filter('cmb_meta_boxes', 'wpcdbs_metaboxes');

// Initialize the metabox class
add_action('init', 'be_initialize_cmb_meta_boxes', 9999);
function be_initialize_cmb_meta_boxes()
{
    if (!class_exists('cmb_Meta_Box')) {
        require_once('init.php');
    }
}

/**
 * Exclude metabox on specific IDs
 * @author Bill Erickson
 * @link https://github.com/jaredatch/Custom-Metaboxes-and-Fields-for-WordPress/wiki/Adding-your-own-show_on-filters
 *
 * @param bool $display
 * @param array $meta_box
 * @return bool display metabox
 */
function be_metabox_exclude_for_id($display, $meta_box)
{
    if ('exclude_id' !== $meta_box['show_on']['key'])
        return $display;

    // If we're showing it based on ID, get the current ID
    if (isset($_GET['post'])) $post_id = $_GET['post'];
    elseif (isset($_POST['post_ID'])) $post_id = $_POST['post_ID'];
    if (!isset($post_id))
        return $display;

    // If current page id is in the included array, do not display the metabox
    $meta_box['show_on']['value'] = !is_array($meta_box['show_on']['value']) ? array($meta_box['show_on']['value']) : $meta_box['show_on']['value'];
    if (in_array($post_id, $meta_box['show_on']['value']))
        return false;
    else
        return true;
}

add_filter('cmb_show_on', 'be_metabox_exclude_for_id', 10, 2);