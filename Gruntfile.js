module.exports = function (grunt) {

    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        concat: {
            dist: {
                src: [
                    'js/libs/*.js',
                    'js/helpers/*.js',
                    'js/widgets/*.js',
                    'js/views/*.js',
                    'js/main/main.js'],
                dest: 'assets/js/scripts.js'
            }
        },

        watch: {
            files: [
                'js/libs/*.js',
                'js/helpers/*.js',
                'js/widgets/*.js',
                'js/views/*.js',
                'js/main/main.js',
                'sass/layout/*.scss',
                'sass/helpers/*.scss',
                'sass/mixing/*.scss',
                'sass/config/*.scss',
                'sass/widgets/*.scss',
                'sass/ie/*.scss',
                'sass/*.scss'],
            tasks: [
                'concat',
                'compass'
            ]

        },

        compass: {
            dist: {
                options: {
                    sassDir: 'sass',
                    cssDir: 'assets/css',
                    raw: 'preferred_syntax = :sass\n',
                    imagesDir:             "assets/images/",
                    generatedImagesDir:    "assets/images/",
                    generatedImagesPath:   "assets/images/",
                    httpGeneratedImagesPath: "../images/"
                }
            }
        },

        uglify: {
            project_production: {
                files: {
                    'assets/js/scripts.min.js': ['assets/js/scripts.js']
                }
            }
        },

        cssmin: {
            project_production: {
                files: {
                    'assets/css/screen.min.css': ['assets/css/screen.css']
                }
            }
        },

        uncss: {
            dist: {
                src: ['html/index.html'],
                dest: 'assets/css/tidy.css',
                options: {
                    report: 'min' // optional: include to report savings
                }
            }
        },

        imagemin: {
            dynamic: {
                options: {
                    optimizationLevel: 3
                },
                files: [
                    {
                        expand: true,
                        cwd: 'assets/images',
                        src: ['**/*.{png,jpg,gif}'],
                        dest: 'assets/images'
                    }
                ]
            }
        }

    });

    // Default task.
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-compass');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-imagemin');
    grunt.loadNpmTasks('grunt-uncss');
    grunt.loadNpmTasks('grunt-shell');

    grunt.registerTask('prod', ['concat','compass', 'imagemin', 'uglify', 'cssmin']);
    grunt.registerTask('default', ['concat','compass', 'imagemin', 'uglify', 'cssmin', 'jshint']);
};