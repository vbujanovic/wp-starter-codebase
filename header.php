<!DOCTYPE html>
<!--[if lt IE 7]>
<html
    xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if IE 7]>
<html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if IE 8]>
<html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?> class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!-->
<html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?> class="no-js"><![endif]-->
<head profile="http://gmpg.org/xfn/11">

    <meta charset="utf-8">

    <meta name="description" content="">

    <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=1">

    <meta name="apple-mobile-web-app-capable" content="yes">

    <meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>"/>

    <?php if (is_search()) { ?>
        <meta name="robots" content="noindex, nofollow"/>
    <?php } ?>

    <title>
        <?php
        if (function_exists('is_tag') && is_tag()) {
            single_tag_title("Tag Archive for &quot;");
            echo '&quot; - ';
        } elseif (is_archive()) {
            wp_title('');
            echo ' Archive - ';
        } elseif (is_search()) {
            echo 'Search for &quot;' . wp_specialchars($s) . '&quot; - ';
        } elseif (!(is_404()) && (is_single()) || (is_page())) {
            wp_title('');
            echo ' - ';
        } elseif (is_404()) {
            echo 'Not Found - ';
        }
        if (is_home()) {
            bloginfo('name');
            echo ' - ';
            bloginfo('description');
        } else {
            bloginfo('name');
        }
        if ($paged > 1) {
            echo ' - page ' . $paged;
        }
        ?>
    </title>

    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon"/>

    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/assets/css/screen.css">

    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>"/>

    <?php if (is_singular()) wp_enqueue_script('comment-reply'); ?>

    <?php wp_head(); ?>

</head>

<body <?php body_class(); ?>>
<div class="header-wrapper">
    <header class="main inner-wrapper">
            <a href="<?php echo esc_url(home_url('/')); ?>" class="logo">
                <img src="<?php
                global $admin_options;
                echo $admin_options['logo']['url'];
                ?>" alt="<?php bloginfo('name'); ?>"/>
            </a>
        <nav id="main-nav" class="main-nav clearfix">
            <?php wp_nav_menu(array('theme_location' => 'main-nav')); ?>
            <a href="#" id="pull-btn">Menu</a>
        </nav>

    </header>
</div>

<div class="content-wrapper">